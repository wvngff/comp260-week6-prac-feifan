﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour{
    
    public float speed = 2f;
    public float force = 10f;
    private Rigidbody rigidbody;

    // Use this for initialization
    void Start(){
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }


    void FixedUpdate() {
            Vector3 dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            rigidbody.velocity = dir * speed;
        
    }
}

